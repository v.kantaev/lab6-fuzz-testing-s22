import { calculateBonuses } from "./bonus-system.js";
const assert = require("assert");


describe('Bonus system tests', () => {
    let app;
    console.log("Tests started");


    test('Standard < 10000 test',  (done) => {
        let program = "Standard";
        let amount = 9999;
        assert.equal(calculateBonuses(program, amount), 1*0.05);
        done();
    });

    test('Standard 10000 test',  (done) => {
        let program = "Standard";
        let amount = 10000;
        assert.equal(calculateBonuses(program, amount), 1.5*0.05);
        done();
    });

    test('Standard < 50000 test',  (done) => {
        let program = "Standard";
        let amount = 49999;
        assert.equal(calculateBonuses(program, amount), 1.5*0.05);
        done();
    });

    test('Standard 50000 test',  (done) => {
        let program = "Standard";
        let amount = 50000;
        assert.equal(calculateBonuses(program, amount), 2*0.05);
        done();
    });


    test('Standard < 100000 test',  (done) => {
        let program = "Standard";
        let amount = 99999;
        assert.equal(calculateBonuses(program, amount), 2*0.05);
        done();
    });

    test('Standard 100000 test',  (done) => {
        let program = "Standard";
        let amount = 100000;
        assert.equal(calculateBonuses(program, amount), 2.5*0.05);
        done();
    });

    test('Standard > 100000 test',  (done) => {
        let program = "Standard";
        let amount = 199999;
        assert.equal(calculateBonuses(program, amount), 2.5*0.05);
        done();
    });

    test('Premium < 10000 test',  (done) => {
        let program = "Premium";
        let amount = 5000;
        assert.equal(calculateBonuses(program, amount), 1*0.1);
        done();
    });

    test('Diamond < 10000 test',  (done) => {
        let program = "Diamond";
        let amount = 5000;
        assert.equal(calculateBonuses(program, amount), 1*0.2);
        done();
    });

    test('Other < 10000 test',  (done) => {
        let program = "Other";
        let amount = 5000;
        assert.equal(calculateBonuses(program, amount), 1*0);
        done();
    });


    console.log('Tests Finished');

});
